# Funcao responsavel por ler os dados do ini
function Get-IniContent ($filePath)
{
    $ini = @{}
    switch -regex -file $FilePath
    {
        "^\[(.+)\]" # Section
        {
            $section = $matches[1]
            $ini[$section] = @{}
            $CommentCount = 0
        }
        "^(;.*)$" # Comment
        {
            $value = $matches[1]
            $CommentCount = $CommentCount + 1
            $name = "Comment" + $CommentCount
            $ini[$section][$name] = $value
        }
        "(.+?)\s*=(.*)" # Key
        {
            $name,$value = $matches[1..2]
            $ini[$section][$name] = $value
        }
    }
    return $ini
}
try {
  # Diretorio Base
  $BaseDir = "C:\TOTVS12\Protheus\Protheus"

  # Diretorio com as includes
  $IncludeFolder = "$BaseDir\include"

  # Caminho do RPO onde os fontes seram compilados
  $BuildPath = "$BaseDir\apo\testing"

  # Caminho do novo RPO
  $DestPath = "$BaseDir\apo\$ENV:CI_COMMIT_SHORT_SHA"

  # Caminho do appserver de producao
  $ProductionAppServer = "$BaseDir\bin\appserver"

  Write-Output "$ProductionAppServer\appserver.ini"

  # Le o arquivo ini e busca o SourcePath atual
  $IniData = Get-IniContent "$ProductionAppServer\appserver.ini"
  $SourcePath = $IniData['environment']['SourcePath']
  Write-Host "RPO Current Folder: $SourcePath"

  # Copia o RPO atual para o diretorio BUILD
  Write-Host "Copying file $SourcePath\TTTM120.rpo to $BuildPath"
  Copy-Item -Path $SourcePath\* -Destination $BuildPath -Recurse -Force

  # Compila os fontes utilizando o appserver
  Set-Location $BaseDir\bin\appserver

  ./appserver.exe -compile -files="$ENV:CI_PROJECT_DIR\api" -includes="$IncludeFolder" -env=testing

  # Executa os testes unitarios
  ./appserver.exe -console -consolelog -run=u_runTests -env=testing

#   if(!$?) {
#     Exit $LASTEXITCODE
#   }

  # Criando estrutura do novo RPO e copia o mesmo para o novo diretorio
  New-Item -ItemType Directory -Path $DestPath
  Copy-Item -Path $BuildPath\* -Destination "$DestPath\TTTM120.rpo" -Recurse -Force

  # Troca a informacao no INI de producao
  Copy-Item -Path "$ProductionAppServer\appserver.ini" -Destination "$ProductionAppServer\appserver_Bkp.ini" -Force

  Write-Host "Updating appserver.ini..."
  Write-Host "Previous content: $SourcePath"
  Write-Host "New content: $DestPath"

  (Get-Content $ProductionAppServer\appserver.ini).replace($SourcePath, $DestPath) | Set-Content $ProductionAppServer\appserver.ini

}
catch
{
      write-host "Caught an exception"
      exit 1
}
